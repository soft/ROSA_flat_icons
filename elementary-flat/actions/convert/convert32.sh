#!/bin/sh

for a in *.svg; do
rsvg-convert -w 32 -f svg ./$a -o ../$a && sed -i 's/32pt/100%/g' ../$a
done
