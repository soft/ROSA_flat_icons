#!/bin/sh

for a in *.svg; do
rsvg-convert -w 48 -f svg ./$a -o ../$a && sed -i 's/48pt/100%/g' ../$a
done
