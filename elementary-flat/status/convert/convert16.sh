#!/bin/sh

for a in *.svg; do
rsvg-convert -w 16 -f svg ./$a -o ../$a && sed -i 's/16pt/100%/g' ../$a
done
