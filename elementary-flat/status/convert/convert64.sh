#!/bin/sh

for a in *.svg; do
rsvg-convert -w 64 -f svg ./$a -o ../$a && sed -i 's/64pt/100%/g' ../$a
done
