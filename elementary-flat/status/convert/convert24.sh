#!/bin/sh

for a in *.svg; do
rsvg-convert -w 24 -f svg ./$a -o ../$a && sed -i 's/24pt/100%/g' ../$a
done
